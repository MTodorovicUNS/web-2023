package services;

import beans.Customer;
import beans.Manager;
import beans.User;
import beans.Worker;
import dao.UserDAO;
import security.JWTSession;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Objects;

@Path("/auth")
public class AuthorizationService {

    @Context
    ServletContext context;

    @PostConstruct
    public void init() {
        if (context.getAttribute("userDAO") == null) {
            String contextPath = context.getRealPath("");
            context.setAttribute("userDAO", new UserDAO(contextPath));
        }
        if (context.getAttribute("JWTSession") == null) {
            context.setAttribute("JWTSession", new JWTSession());
        }
    }

    @POST
    @Path("/login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(User user, @Context HttpServletRequest request) {
        UserDAO userDao = (UserDAO) context.getAttribute("userDAO");
        JWTSession jwtSession = (JWTSession) context.getAttribute("JWTSession");

        User admin = userDao.getAdmin(user.getUsername());
        Manager manager = userDao.getManager(user.getUsername());
        Customer customer = userDao.getCustomer(user.getUsername());
        Worker worker = userDao.getWorker(user.getUsername());

        if (customer == null && admin == null && worker == null && manager == null) {
            return Response.status(400).entity("Invalid username and/or password").build();
        }
        if (Objects.nonNull(customer)) {
            jwtSession.createJWT(customer);
            request.getSession().setAttribute("user", customer);
            return Response.ok(customer).build();
        }
        if (Objects.nonNull(admin)) {
            jwtSession.createJWT(admin);
            request.getSession().setAttribute("user", admin);
            return Response.ok(admin).build();
        }
        if (Objects.nonNull(manager)) {
            jwtSession.createJWT(manager);
            request.getSession().setAttribute("user", manager);
            return Response.ok(manager).build();
        }
        if (Objects.nonNull(worker)) {
            jwtSession.createJWT(worker);
            request.getSession().setAttribute("user", worker);
            return Response.ok(worker).build();
        }
        return Response.status(400).entity("Bad request!").build();
    }


    @POST
    @Path("/logout")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response logout(@Context HttpServletRequest request) {
        JWTSession jwtSession = (JWTSession) context.getAttribute("JWTSession");
        UserDAO userDAO = (UserDAO) context.getAttribute("userDAO");
        User user = jwtSession.checkJWT(request, userDAO);
        if (user != null) {
            user.setJwt("");
            return Response.ok(user).build();
        }
        return Response.status(400).entity("Not logged in").build();
    }

    @GET
    @Path("/currentUser")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public User login(@Context HttpServletRequest request) {
        return (User) request.getSession().getAttribute("user");
    }

    @POST
    @Path("/register")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response register(Customer customer) {
        UserDAO userDAO = (UserDAO) context.getAttribute("userDAO");
        String path = context.getRealPath("");
        try {
            Customer k = userDAO.addUser(customer, path);
            if (k == null) {
                return Response.status(400).entity("A user with the given username already exists!").build();
            }
            return Response.ok(k).build();
        } catch (Exception e) {
            return Response.status(500).entity("Error while registering!").build();
        }
    }

}
