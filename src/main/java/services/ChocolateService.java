package services;

import beans.Chocolate;
import beans.ChocolateFactory;
import dao.ChocolateFactoryDAO;
import dto.ChocolateDTO;
import dto.ChocolateFactoryDTO;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Path("/chocolate")
public class ChocolateService {
    @Context
    ServletContext context;

    @PostConstruct
    public void init() {
        if (context.getAttribute("chocolateFactoryDAO") == null) {
            String contextPath = context.getRealPath("");
            context.setAttribute("chocolateFactoryDAO", new ChocolateFactoryDAO(contextPath));
        }
    }

    @GET
    @Path("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@Context HttpServletRequest httpServletRequest,  @QueryParam("factoryName") String factoryName) {
        ChocolateFactoryDAO chocolateFactoryDAO = (ChocolateFactoryDAO) context.getAttribute("chocolateFactoryDAO");
        return Response.ok(chocolateFactoryDAO.getAllChocolates(factoryName)).build();
    }

    @GET
    @Path("/name")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@Context HttpServletRequest httpServletRequest,  @QueryParam("name") String name, @QueryParam("factoryName") String factoryName) {
        ChocolateFactoryDAO chocolateFactoryDAO = (ChocolateFactoryDAO) context.getAttribute("chocolateFactoryDAO");
        return Response.ok(chocolateFactoryDAO.getChocolateByName(name, factoryName)).build();
    }

    @POST
    @Path("/add")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addChocolate(ChocolateDTO chocolateDTO) {
        ChocolateFactoryDAO chocolateFactoryDAO = (ChocolateFactoryDAO) context.getAttribute("chocolateFactoryDAO");
        String path = context.getRealPath("");
        try {
            Chocolate k = chocolateFactoryDAO.addChocolate(chocolateDTO, path);
            if (k == null) {
                return Response.status(400).entity("A chocolate already exists!").build();
            }
            return Response.ok(k).build();
        } catch (Exception e) {
            return Response.status(500).entity("Error while registering!").build();
        }
    }

    @POST
    @Path("/factory/add")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response addChocolateFactory(ChocolateFactoryDTO chocolateFactory) {
        ChocolateFactoryDAO chocolateFactoryDAO = (ChocolateFactoryDAO) context.getAttribute("chocolateFactoryDAO");
        String path = context.getRealPath("");
        try {
            ChocolateFactory k = chocolateFactoryDAO.addChocolateFactory(chocolateFactory, path);
            if (k == null) {
                return Response.status(400).entity("A chocolate factory already exists!").build();
            }
            return Response.ok(k).build();
        } catch (Exception e) {
            return Response.status(500).entity("Error while registering!").build();
        }
    }

    @GET
    @Path("/factory/all")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@Context HttpServletRequest httpServletRequest) {
        ChocolateFactoryDAO chocolateFactoryDAO = (ChocolateFactoryDAO) context.getAttribute("chocolateFactoryDAO");
        return Response.ok(chocolateFactoryDAO.getAllChocolateFactories()).build();
    }

    @GET
    @Path("/factory/name")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll(@QueryParam("name") String name) {
        ChocolateFactoryDAO chocolateFactoryDAO = (ChocolateFactoryDAO) context.getAttribute("chocolateFactoryDAO");
        return Response.ok(chocolateFactoryDAO.getByFactoryName(name)).build();
    }


    @PUT
    @Path("/factory/edit")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editChocolateFactory(@Context HttpServletRequest httpServletRequest, ChocolateFactoryDTO chocolateFactory) {
        ChocolateFactoryDAO chocolateFactoryDAO = (ChocolateFactoryDAO) context.getAttribute("chocolateFactoryDAO");
        String path = context.getRealPath("");
        try {
            ChocolateFactory k = chocolateFactoryDAO.editChocolateFactory(chocolateFactory, path);
            if (k == null) {
                return Response.status(400).entity("A chocolate factory already exists!").build();
            }
            return Response.ok(k).build();
        } catch (Exception e) {
            return Response.status(500).entity("Error while registering!").build();
        }
    }

    @GET
    @Path("/factory/search")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response searchChocolateFactories(@QueryParam("name") String name, @QueryParam("chocolateName") String chocolateName,
                                      @QueryParam("location") String location, @QueryParam("rate") String rate){
        ChocolateFactoryDAO chocolateFactoryDAO = (ChocolateFactoryDAO) context.getAttribute("chocolateFactoryDAO");
        Collection<ChocolateFactory> results = chocolateFactoryDAO.searchChocolateFactories(name, chocolateName, location, rate);
        if (results == null) {
            return Response.status(400).entity("Error!").build();
        }
        return Response.ok(results).build();
    }

    @GET
    @Path("/factory/filter")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFilteredAndSortedFactories(
            @QueryParam("name") @DefaultValue("") String name,
            @QueryParam("location") @DefaultValue("") String location,
            @QueryParam("rating") @DefaultValue("0") float rating,
            @QueryParam("sortBy") @DefaultValue("name") String sortBy,
            @QueryParam("sortOrder") @DefaultValue("asc") String sortOrder) {

        ChocolateFactoryDAO chocolateFactoryDAO = (ChocolateFactoryDAO) context.getAttribute("chocolateFactoryDAO");

        Collection<ChocolateFactory> allFactories = chocolateFactoryDAO.getAll();

        Collection<ChocolateFactory> filteredFactories = allFactories.stream()
                .filter(factory -> factory.getName().toLowerCase().contains(name.toLowerCase()))
                .filter(factory -> factory.getLocation().getAddress().toLowerCase().contains(location.toLowerCase()))
                .filter(factory -> factory.getReview() >= rating)
                .collect(Collectors.toList());

        List<ChocolateFactory> sortedFactories = filteredFactories.stream()
                .sorted((f1, f2) -> {
                    int comparison = 0;
                    switch (sortBy) {
                        case "name":
                            comparison = f1.getName().compareToIgnoreCase(f2.getName());
                            break;
                        case "location":
                            comparison = f1.getLocation().getAddress().compareToIgnoreCase(f2.getLocation().getAddress());
                            break;
                        case "rating":
                            comparison = Float.compare(f1.getReview(), f2.getReview());
                            break;
                    }
                    return "desc".equalsIgnoreCase(sortOrder) ? -comparison : comparison;
                })
                .collect(Collectors.toList());

        return Response.ok(sortedFactories).build();
    }
}
