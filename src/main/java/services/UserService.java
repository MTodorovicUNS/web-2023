package services;

import beans.ChocolateFactory;
import beans.Manager;
import beans.User;
import beans.Worker;
import dao.ChocolateFactoryDAO;
import dao.UserDAO;
import dto.ChocolateFactoryDTO;
import dto.WorkerDTO;

import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Path("/user")
public class UserService {
    @Context
    ServletContext context;

    @PostConstruct
    public void init() {
        if (context.getAttribute("userDAO") == null) {
            String contextPath = context.getRealPath("");
            context.setAttribute("userDAO", new UserDAO(contextPath));
        }
    }

    @GET
    @Path("/all")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed("ADMIN")
    public Response getAll(@Context HttpServletRequest request) {
        UserDAO userDAO = (UserDAO) context.getAttribute("userDAO");
        return Response.ok(userDAO.getAllUsers()).build();
    }

    @PUT
    @Path("/update")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateUser(@Context HttpServletRequest httpServletRequest, User user) {
        UserDAO userDAO = (UserDAO) context.getAttribute("userDAO");
        String path = context.getRealPath("");
        try {
            User u = userDAO.updateUser(user, path);
            if (u == null) {
                return Response.status(400).entity("Update failed!").build();
            }
            return Response.ok(u).build();
        } catch (Exception e) {
            return Response.status(500).entity("Error while registering!").build();
        }
    }

    @GET
    @Path("/available/managers")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed("ADMIN")
    public Response getAllAvailableManagers(@Context HttpServletRequest request) {
        UserDAO userDAO = (UserDAO) context.getAttribute("userDAO");
        List<Manager> managers = userDAO.getAllManagers().stream()
                .filter(user -> Objects.isNull(user.getChocolateFactory()))
                .collect(Collectors.toList());
        return Response.ok(managers).build();
    }


    @POST
    @Path("/manager/add")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed("ADMIN")
    public Response addManager(Manager manager) {
        UserDAO userDAO = (UserDAO) context.getAttribute("userDAO");
        String path = context.getRealPath("");
        try {
            Manager m = userDAO.addManager(manager, path);
            if (m == null) {
                return Response.status(400).entity("A user with the given username already exists!").build();
            }
            return Response.ok(m).build();
        } catch (Exception e) {
            return Response.status(500).entity("Error while registering!").build();
        }
    }

    @POST
    @Path("/worker/add")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed("MANAGER")
    public Response addWorker(WorkerDTO worker) {
        UserDAO userDAO = (UserDAO) context.getAttribute("userDAO");
        String path = context.getRealPath("");
        try {
            Worker m = userDAO.addWorker(worker, path);
            if (m == null) {
                return Response.status(400).entity("A user with the given username already exists!").build();
            }
            return Response.ok(m).build();
        } catch (Exception e) {
            return Response.status(500).entity("Error while registering!").build();
        }
    }
    @GET
    @Path("/worker/all")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @RolesAllowed("MANAGER")
    public Response getAllWorkers(Worker worker, @QueryParam("factoryName") String factoryName) {
        UserDAO userDAO = (UserDAO) context.getAttribute("userDAO");
        List<Worker> workers = userDAO.getAllWorkers().stream()
                .filter(user -> Objects.equals(factoryName, user.getChocolateFactory().getName()))
                .collect(Collectors.toList());
        return Response.ok(workers).build();
    }
}
