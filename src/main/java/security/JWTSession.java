package security;

import beans.User;

import dao.UserDAO;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import javax.servlet.http.HttpServletRequest;
import java.security.Key;
import java.util.Date;

public class JWTSession {

    static Key key = Keys.secretKeyFor(SignatureAlgorithm.HS256);

    public void createJWT(User user) {
        String jws = Jwts.builder().setSubject(user.getUsername()).setExpiration(new Date(new Date().getTime() + 30 * 6000 * 10L)).setIssuedAt(new Date()).signWith(key).compact();
        user.setJwt(jws);
    }

    public User checkJWT(HttpServletRequest httpServletRequest, UserDAO userDAO) {
        String auth = httpServletRequest.getHeader("Authorization");

        if ((auth != null) && (auth.contains("Bearer "))) {
            String jwt = auth.substring(auth.indexOf("Bearer ") + 7);

            try {
                Jws<Claims> claims = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(jwt);
                String username = claims.getBody().getSubject();
                User user = userDAO.getByUsername(username);
                if (user == null)
                    user = userDAO.getAdmin(username);
                if (user == null)
                    user = userDAO.getManager(username);
                return user;
            } catch (Exception e) {
                return null;
            }
        }
        return null;
    }
}

