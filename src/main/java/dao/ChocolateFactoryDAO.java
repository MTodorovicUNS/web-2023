package dao;

import beans.*;
import dto.ChocolateDTO;
import dto.ChocolateFactoryDTO;
import dto.ChocolateFactoryReturnDTO;
import enums.RoleEnum;
import enums.TypeEnum;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class ChocolateFactoryDAO {

    private final HashMap<String, ChocolateFactory> chocolateFactories;
    private final HashMap<String, Chocolate> chocolates;
    private final HashMap<String, Comment> comments;
    private final HashMap<String, Purchases> purchases;
    private HashMap<String, Manager> managers;
    private HashMap<String, Customer> customers;

    Logger logger = Logger.getLogger(ChocolateFactoryDAO.class.getName());

    public ChocolateFactoryDAO(String contextPath) {
        chocolateFactories = new HashMap<>();
        chocolates = new HashMap<>();
        managers = new HashMap<>();
        customers = new HashMap<>();
        comments = new HashMap<>();
        purchases = new HashMap<>();

        loadChocolateFactories(contextPath);
        loadChocolates(contextPath);
        loadChocolateToFactories(contextPath);
        loadManagers(contextPath);
        loadCustomers(contextPath);
        loadComments(contextPath);
    }

    public List<ChocolateFactory> getAll() {
        return new ArrayList<>(findAll());
    }

    public ChocolateFactory getByName(String factoryName) {
        for (ChocolateFactory chocolateF : findAll()) {
            if (chocolateF.getName().toLowerCase().contains(factoryName.toLowerCase())) {
                return chocolateF;
            }
        }
        return null;
    }

    public ChocolateFactoryReturnDTO getByFactoryName(String factoryName) {
        ChocolateFactoryReturnDTO chocolateFactoryReturnDTO = new ChocolateFactoryReturnDTO();
        for (ChocolateFactory chocolateF : findAll()) {
            if (chocolateF.getName().toLowerCase().contains(factoryName.toLowerCase())) {
                chocolateFactoryReturnDTO.setName(chocolateF.getName());
                chocolateFactoryReturnDTO.setLocation(chocolateF.getLocation());
                chocolateFactoryReturnDTO.setLogo(chocolateF.getLogo());
                chocolateFactoryReturnDTO.setReview(chocolateF.getReview());
                chocolateFactoryReturnDTO.setIsWorking(chocolateF.getIsWorking());
                chocolateFactoryReturnDTO.setWorkingHours(chocolateF.getWorkingHours());
            }
        }
        for(Manager manager : managers.values()) {
            if(manager.getChocolateFactory().getName().equalsIgnoreCase(factoryName)) {
                chocolateFactoryReturnDTO.setManager(manager);
                break;
            }
        }
        List<Chocolate> chocolatesList = new ArrayList<>();
        for(Chocolate chocolate : chocolates.values()) {
            if(chocolate.getFactory().getName().equalsIgnoreCase(factoryName)) {
                chocolatesList.add(chocolate);
            }
        }
        List<Comment> commentList = new ArrayList<>();
        for(Comment comment : comments.values()) {
            if(comment.getChocolateFactory().getName().equalsIgnoreCase(factoryName)) {
                commentList.add(comment);
            }
        }
        chocolateFactoryReturnDTO.setChocolates(chocolatesList);
        chocolateFactoryReturnDTO.setComments(commentList);
        return chocolateFactoryReturnDTO;
    }

    public Collection<ChocolateFactory> findAll() {
        return chocolateFactories.values();
    }


    private void loadChocolateFactories(String path) {
        File file = new File(path + "data\\chocolateFactory.csv");
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parameters = line.split(",");
                String name = parameters[0];
                String chocolatesParam = parameters[1];
                String workingHours = parameters[2];
                String isWorking = parameters[3];
                String longitude = parameters[4];
                String latitude = parameters[5];
                String location = parameters[6];
                String logo = parameters[7];
                String review = parameters[8];

                List<Chocolate> chocolatesForFactory = new ArrayList<>();

//                String[] chocolatesList = chocolatesParam.split(";");
//                for (String chocolate : chocolatesList) {
//                    Chocolate chocolateByName = getChocolateByName(chocolate);
//                    chocolatesForFactory.add(chocolateByName);
//                }

                Location locationObject = new Location();
                locationObject.setAddress(location);
                locationObject.setLatitude(Float.parseFloat(latitude));
                locationObject.setLongitude(Float.parseFloat(longitude));

                ChocolateFactory chocolateFactory = new ChocolateFactory(name, chocolatesForFactory, workingHours, Boolean.parseBoolean(isWorking),
                        locationObject, logo, Float.parseFloat(review));
                chocolateFactories.put(name, chocolateFactory);
            }
        } catch (IOException e) {
            logger.log(Level.WARNING, e.getMessage());
        }
    }
    private void loadComments(String path) {
        File file = new File(path + "data\\comment.csv");
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parameters = line.split(",");
                String id = parameters[0];
                String customerUsername = parameters[1];
                String factoryName = parameters[2];
                String text = parameters[3];
                String rate = parameters[4];
                String accepted = parameters[5];

                Customer customer = getCustomer(customerUsername);
                ChocolateFactory chocolateFactory = getByName(factoryName);

                Comment comment = new Comment(id, customer, chocolateFactory, text, Float.parseFloat(rate), Boolean.parseBoolean(accepted));
                comments.put(id, comment);
            }
        } catch (IOException e) {
            logger.log(Level.WARNING, e.getMessage());
        }
    }

    private void loadChocolateToFactories(String path) {
        File file = new File(path + "data\\chocolateFactory.csv");
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parameters = line.split(",");
                String name = parameters[0];
                String chocolatesParam = parameters[1];

                List<Chocolate> chocolatesForFactory = new ArrayList<>();

                String[] chocolatesList = chocolatesParam.split(";");
                for (String chocolate : chocolatesList) {
                    Chocolate chocolateByName = getChocolateByName(chocolate);
                    chocolatesForFactory.add(chocolateByName);
                }

                ChocolateFactory chocolateFactory = getByName(name);
                chocolateFactory.setChocolates(chocolatesForFactory);

                chocolateFactories.put(name, chocolateFactory);
            }
        } catch (IOException e) {
            logger.log(Level.WARNING, e.getMessage());
        }
    }


    public List<Chocolate> getAllChocolates(String factoryName) {
        List<Chocolate> chocolatesList = new ArrayList<>();
        for (Chocolate chocolate : findAllChocolates()) {
            if (chocolate.getFactory().getName().toLowerCase().contains(factoryName.toLowerCase())) {
                chocolatesList.add(chocolate);
            }
        }
        return chocolatesList;
    }

    public Chocolate getChocolateByName(String name, String factoryName) {
        for (Chocolate chocolate : findAllChocolates()) {
            if (chocolate.getFactory().getName().toLowerCase().contains(factoryName.toLowerCase()) &&
            chocolate.getName().toLowerCase().contains(name.toLowerCase())) {
                return chocolate;
            }
        }
        return null;
    }

    public Chocolate getChocolateByName(String name) {
        for (Chocolate chocolate : findAllChocolates()) {
            if (chocolate.getName().toLowerCase().contains(name.toLowerCase())) {
                return chocolate;
            }
        }
        return null;
    }

    public Collection<Chocolate> findAllChocolates() {
        return chocolates.values();
    }

    private void loadChocolates(String path) {
        File file = new File(path + "data\\chocolate.csv");
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parameters = line.split(",");
                String name = parameters[0];
                String price = parameters[1];
                String type = parameters[2];
                String factory = parameters[3];
                String flavor = parameters[4];
                String weight = parameters[5];
                String description = parameters[6];
                String image = parameters[7];
                String inStock = parameters[8];
                String quantity = parameters[9];
                ChocolateFactory chocolateFactory = getByName(factory);
                Chocolate chocolate = new Chocolate(name, Double.parseDouble(price), type, chocolateFactory, flavor, Float.parseFloat(weight), description,
                        image, Boolean.parseBoolean(inStock), Integer.parseInt(quantity));
                chocolates.put(name, chocolate);
            }
        } catch (IOException e) {
            logger.log(Level.WARNING, e.getMessage());
        }
    }

    public ChocolateFactory addChocolateFactory(ChocolateFactoryDTO chocolateFactory, String path) {
        String chocolateFactoryPath = path + "\\data\\chocolateFactory.csv";
        String managersPath = path + "\\data\\managers.csv";
        String usersPath = path + "\\data\\users.csv";

        ChocolateFactory factory = new ChocolateFactory();

        factory.setName(chocolateFactory.getName());

        Location locationObject = new Location();
        locationObject.setAddress(chocolateFactory.getLocation());
        locationObject.setLongitude(0);
        locationObject.setLatitude(0);

        factory.setLocation(locationObject);
        factory.setWorkingHours(chocolateFactory.getWorkingHours());
        factory.setLogo(chocolateFactory.getLogo());
        factory.setReview(Float.parseFloat(chocolateFactory.getReview()));
        factory.setIsWorking(Boolean.parseBoolean(chocolateFactory.getIsWorking()));

        if(Objects.nonNull(chocolateFactory.getManagerName())) {
            Manager manager = getManager(chocolateFactory.getManagerName());
            manager.setChocolateFactory(factory);
            writeManagersToFile(managersPath);
        } else {
            Manager manager = new Manager();
            manager.setFirstName(chocolateFactory.getFirstName());
            manager.setLastName(chocolateFactory.getLastName());
            manager.setDate(chocolateFactory.getDate());
            manager.setGender(chocolateFactory.getGender());
            manager.setDeleted(false);
            manager.setChocolateFactory(factory);
            writeManagerToFile(managersPath, manager);
            writeUserToFile(usersPath, manager);
        }

        chocolateFactories.put(chocolateFactory.getName(), factory);
        writeChocolateFactoryToFile(chocolateFactoryPath, factory);

        return factory;
    }

    private void writeChocolateFactoryToFile(String chocolateFactoryPath, ChocolateFactory chocolateFactory) {
        try (Writer writer = new BufferedWriter(new FileWriter(chocolateFactoryPath, true))) {
            writer.append(chocolateFactory.getName());
            writer.append(",");
            writer.append("");
            writer.append(",");
            writer.append(chocolateFactory.getWorkingHours());
            writer.append(",");
            writer.append(String.valueOf(chocolateFactory.getIsWorking()));
            writer.append(",");
            writer.append(String.valueOf(chocolateFactory.getLocation().getLatitude()));
            writer.append(",");
            writer.append(String.valueOf(chocolateFactory.getLocation().getLongitude()));
            writer.append(",");
            writer.append(chocolateFactory.getLocation().getAddress());
            writer.append(",");
            writer.append(chocolateFactory.getLogo());
            writer.append(",");
            writer.append(String.valueOf(chocolateFactory.getReview()));
            writer.append("\n");
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void loadManagers(String path) {
        File file = new File(path + "data\\managers.csv");
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parameters = line.split(",");
                String username = parameters[0];
                String password = parameters[1];
                String firstName = parameters[2];
                String lastName = parameters[3];
                String gender = parameters[4];
                String date = parameters[5];
                String role = parameters[6];
                String deleted = parameters[7];
                String factoryName = parameters[8];
                ChocolateFactory chocolateFactory = getByName(factoryName);
                Manager manager = new Manager(username, password, firstName, lastName, gender, date, RoleEnum.valueOf(role), Boolean.parseBoolean(deleted), chocolateFactory);
                managers.put(username, manager);
            }
        } catch (IOException e) {
            logger.log(Level.WARNING, e.getMessage());
        }
    }

    public void writeUserToFile(String path, User user) {
        try (Writer writer = new BufferedWriter(new FileWriter(path, true))) {
            writer.append(user.getFirstName());
            writer.append(",");
            writer.append(user.getLastName());
            writer.append(",");
            writer.append(user.getUsername());
            writer.append(",");
            writer.append(user.getPassword());
            writer.append(",");
            writer.append(user.getGender());
            writer.append(",");
            writer.append(user.getDate());
            writer.append(",");
            writer.append(user.getRole().name());
            writer.append(",");
            writer.append(String.valueOf(user.getDeleted()));
            writer.append("\n");
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void writeManagerToFile(String path, Manager manager) {
        try (Writer writer = new BufferedWriter(new FileWriter(path, true))) {
            writer.append(manager.getFirstName());
            writer.append(",");
            writer.append(manager.getLastName());
            writer.append(",");
            writer.append(manager.getUsername());
            writer.append(",");
            writer.append(manager.getPassword());
            writer.append(",");
            writer.append(manager.getGender());
            writer.append(",");
            writer.append(manager.getDate());
            writer.append(",");
            writer.append(manager.getRole().name());
            writer.append(",");
            writer.append(String.valueOf(manager.getDeleted()));
            writer.append(",");
            if (manager.getChocolateFactory() != null) {
                writer.append(manager.getChocolateFactory().getName());
            } else {
                writer.append("n");
            }
            writer.append("\n");
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void writeManagersToFile(String path) {
        try (Writer writer = new BufferedWriter(new FileWriter(path))) {
            for(Manager manager : managers.values()) {
                writer.append(manager.getUsername());
                writer.append(",");
                writer.append(manager.getPassword());
                writer.append(",");
                writer.append(manager.getFirstName());
                writer.append(",");
                writer.append(manager.getLastName());
                writer.append(",");
                writer.append(manager.getGender());
                writer.append(",");
                writer.append(manager.getDate());
                writer.append(",");
                writer.append(manager.getRole().name());
                writer.append(",");
                writer.append(String.valueOf(manager.getDeleted()));
                writer.append(",");
                if (manager.getChocolateFactory() != null) {
                    writer.append(manager.getChocolateFactory().getName());
                }
            }
            writer.append("\n");
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Manager getManager(String username) {
        if (managers.containsKey(username)) {
            return managers.get(username);
        }
        return null;
    }
    public Customer getCustomer(String username) {
        if (customers.containsKey(username)) {
            return customers.get(username);
        }
        return null;
    }

    public Object getAllChocolateFactories() {
        List<ChocolateFactory> chocolateFactoryList = new ArrayList<>();
        for (ChocolateFactory chocolateFactory : findAll()) {
            chocolateFactoryList.add(chocolateFactory);
        }
        return chocolateFactoryList;
    }

    public ChocolateFactory editChocolateFactory(ChocolateFactoryDTO chocolateFactory, String path) {
        String chocolateFactoryPath = path + "\\data\\chocolateFactory.csv";

        ChocolateFactory factory = getByName(chocolateFactory.getName());

        factory.setName(chocolateFactory.getName());

        Location locationObject = new Location();
        locationObject.setAddress(chocolateFactory.getLocation());
        locationObject.setLongitude(0);
        locationObject.setLatitude(0);

        factory.setLocation(locationObject);
        factory.setWorkingHours(chocolateFactory.getWorkingHours());
        factory.setLogo(chocolateFactory.getLogo());
        factory.setReview(Float.parseFloat(chocolateFactory.getReview()));
        factory.setIsWorking(Boolean.parseBoolean(chocolateFactory.getIsWorking()));

        writeChocolateFactoriesToFile(chocolateFactoryPath);

        return factory;
    }

    private void writeChocolateFactoriesToFile(String chocolateFactoryPath) {
        try (Writer writer = new BufferedWriter(new FileWriter(chocolateFactoryPath))) {
            for(ChocolateFactory chocolateFactory : chocolateFactories.values()) {
                writer.append(chocolateFactory.getName());
                writer.append(",");
                writer.append("");
                writer.append(",");
                writer.append(chocolateFactory.getWorkingHours());
                writer.append(",");
                writer.append(String.valueOf(chocolateFactory.getIsWorking()));
                writer.append(",");
                writer.append(String.valueOf(chocolateFactory.getLocation().getLatitude()));
                writer.append(",");
                writer.append(String.valueOf(chocolateFactory.getLocation().getLongitude()));
                writer.append(",");
                writer.append(chocolateFactory.getLocation().getAddress());
                writer.append(",");
                writer.append(chocolateFactory.getLogo());
                writer.append(",");
                writer.append(String.valueOf(chocolateFactory.getReview()));
            }
            writer.append("\n");
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Chocolate addChocolate(ChocolateDTO chocolateDTO, String path) {
        String chocolatePath = path + "\\data\\chocolate.csv";

        Chocolate chocolate = new Chocolate();

        chocolate.setName(chocolateDTO.getName());
        chocolate.setDescription(chocolateDTO.getDescription());
        chocolate.setFlavor(chocolateDTO.getFlavor());
        chocolate.setImage(chocolateDTO.getImage());
        chocolate.setInStock(chocolateDTO.isInStock());
        chocolate.setPrice(chocolateDTO.getPrice());
        chocolate.setWeight(chocolateDTO.getWeight());
        chocolate.setPrice(chocolateDTO.getPrice());
        chocolate.setQuantity(chocolateDTO.getQuantity());
        chocolate.setType(chocolateDTO.getType());

        ChocolateFactory chocolateFactory = getByName(chocolateDTO.getFactoryName());
        chocolate.setFactory(chocolateFactory);

        chocolates.put(chocolateDTO.getName(), chocolate);
        writeChocolateToFile(chocolatePath, chocolate);

        return chocolate;
    }

    private void writeChocolateToFile(String chocolatePath, Chocolate chocolate) {
        try (Writer writer = new BufferedWriter(new FileWriter(chocolatePath, true))) {
            writer.append(chocolate.getName());
            writer.append(",");
            writer.append(String.valueOf(chocolate.getPrice()));
            writer.append(",");
            writer.append(chocolate.getType());
            writer.append(",");
            writer.append(chocolate.getFactory().getName());
            writer.append(",");
            writer.append(chocolate.getFlavor());
            writer.append(",");
            writer.append(String.valueOf(chocolate.getWeight()));
            writer.append(",");
            writer.append(chocolate.getDescription());
            writer.append(",");
            writer.append(chocolate.getImage());
            writer.append(",");
            writer.append(String.valueOf(chocolate.isInStock()));
            writer.append(",");
            writer.append(String.valueOf(chocolate.getQuantity()));
            writer.append("\n");
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void writeChocolatesToFile(String chocolatePath) {
        try (Writer writer = new BufferedWriter(new FileWriter(chocolatePath))) {
            for(Chocolate chocolate : chocolates.values()) {
                writer.append(chocolate.getName());
                writer.append(",");
                writer.append(String.valueOf(chocolate.getPrice()));
                writer.append(",");
                writer.append(chocolate.getType());
                writer.append(",");
                writer.append(chocolate.getFactory().getName());
                writer.append(",");
                writer.append(String.valueOf(chocolate.getWeight()));
                writer.append(",");
                writer.append(chocolate.getDescription());
                writer.append(",");
                writer.append(chocolate.getImage());
                writer.append(",");
                writer.append(String.valueOf(chocolate.isInStock()));
                writer.append(",");
                writer.append(String.valueOf(chocolate.getQuantity()));
            }
            writer.append("\n");
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void loadCustomers(String path) {
        File file = new File(path + "data\\customers.csv");
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parameters = line.split(",");
                String username = parameters[0];
                String password = parameters[1];
                String firstName = parameters[2];
                String lastName = parameters[3];
                String gender = parameters[4];
                String date = parameters[5];
                String role = parameters[6];
                String deleted = parameters[7];
                String points = parameters[8];
                String type = parameters[9];

                CustomerType customerType = new CustomerType();
                customerType.setType(TypeEnum.valueOf(type));
                customerType.setDiscount(0);
                customerType.setPoints(0);

                Customer customer = new Customer(username, password, firstName, lastName, gender, date, RoleEnum.valueOf(role),
                        Boolean.parseBoolean(deleted), Integer.parseInt(points), customerType);
                customers.put(username, customer);
            }
        } catch (IOException e) {
            logger.log(Level.WARNING, e.getMessage());
        }
    }

    public Collection<ChocolateFactory> searchChocolateFactories(String name, String chocolateName, String location, String rate) {
        return chocolateFactories.values().stream()
                .filter(factory -> {
                    if (name != null && !name.isEmpty()) {
                        if (!factory.getName().toLowerCase().contains(name.toLowerCase())) {
                            return false;
                        }
                    }
                    if (chocolateName != null && !chocolateName.isEmpty()) {
                        boolean foundChocolate = factory.getChocolates().stream()
                                .anyMatch(chocolate -> chocolate.getName().toLowerCase().contains(chocolateName.toLowerCase()));
                        if (!foundChocolate) {
                            return false;
                        }
                    }
                    if (location != null && !location.isEmpty()) {
                        if (!(factory.getLocation().getAddress().toLowerCase().contains(location.toLowerCase()))) {
                            return false;
                        }
                    }
                    if (rate != null && !rate.isEmpty()) {
                        if (factory.getReview() < Float.parseFloat(rate)) {
                            return false;
                        }
                    }

                    return true;
                })
                .collect(Collectors.toList());
    }
}

