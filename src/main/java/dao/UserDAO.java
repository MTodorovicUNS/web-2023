package dao;

import beans.*;
import com.sun.corba.se.spi.orbutil.threadpool.Work;
import dto.WorkerDTO;
import enums.RoleEnum;
import enums.TypeEnum;

import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserDAO {

    Logger logger = Logger.getLogger(UserDAO.class.getName());
    private Map<String, User> users;
    private HashMap<String, Manager> managers;
    private HashMap<String, Worker> workers;
    private HashMap<String, Customer> customers;
    private User admininstrator;
    private ChocolateFactoryDAO chocolateFactoryDAO;

    public UserDAO(String contextPath) {
        admininstrator = new User();
        users = new HashMap<>();
        managers = new HashMap<>();
        workers = new HashMap<>();
        customers = new HashMap<>();
        chocolateFactoryDAO = new ChocolateFactoryDAO(contextPath);

        loadUsers(contextPath);
        loadManagers(contextPath);
        loadWorkers(contextPath);
    }

    private static String NullToString(Integer points) {
        return points == null ? "0" : points.toString();
    }

    public User find(String username, String password) {
        if (!users.containsKey(username)) {
            return null;
        }
        User user = users.get(username);
        if (!user.getPassword().equals(password)) {
            return null;
        }
        return user;
    }

    public Collection<User> findAll() {
        return users.values();
    }

    private void loadUsers(String path) {
        File file = new File(path + "data\\users.csv");
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parameters = line.split(",");
                String firstName = parameters[2];
                String lastName = parameters[3];
                String username = parameters[0];
                String password = parameters[1];
                String gender = parameters[4];
                String date = parameters[5];
                String role = parameters[6];
                String deleted = parameters[7];
                User user = new User(username, password, firstName, lastName, gender, date, RoleEnum.valueOf(role), Boolean.parseBoolean(deleted));
                users.put(username, user);
            }
        } catch (IOException e) {
            logger.log(Level.WARNING, e.getMessage());
        }
    }

    private void loadWorkers(String path) {
        File file = new File(path + "data\\workers.csv");
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parameters = line.split(",");
                String username = parameters[0];
                String password = parameters[1];
                String firstName = parameters[2];
                String lastName = parameters[3];
                String gender = parameters[4];
                String date = parameters[5];
                String role = parameters[6];
                String deleted = parameters[7];
                String factoryName = "";
                if(Objects.equals(parameters.length, 9)) {
                    factoryName = parameters[8];
                }

                ChocolateFactory chocolateFactory = chocolateFactoryDAO.getByName(factoryName);
                Worker worker = new Worker(username, password, firstName, lastName, gender, date, RoleEnum.valueOf(role), Boolean.parseBoolean(deleted), chocolateFactory);
                workers.put(username, worker);
            }
        } catch (IOException e) {
            logger.log(Level.WARNING, e.getMessage());
        }
    }

    private void loadManagers(String path) {
        File file = new File(path + "data\\managers.csv");
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parameters = line.split(",");
                String username = parameters[0];
                String password = parameters[1];
                String firstName = parameters[2];
                String lastName = parameters[3];
                String gender = parameters[4];
                String date = parameters[5];
                String role = parameters[6];
                String deleted = parameters[7];
                String factoryName = parameters[8];
                ChocolateFactory chocolateFactory = chocolateFactoryDAO.getByName(factoryName);
                Manager manager = new Manager(username, password, firstName, lastName, gender, date, RoleEnum.valueOf(role), Boolean.parseBoolean(deleted), chocolateFactory);
                managers.put(username, manager);
            }
        } catch (IOException e) {
            logger.log(Level.WARNING, e.getMessage());
        }
    }

    public User getByUsername(String username) {
        if (users.containsKey(username)) {
            return users.get(username);
        }
        return null;
    }

    public User getAdmin(String username) {
        if (users.containsKey(username) && users.get(username).getRole().equals(RoleEnum.ADMIN)) {
            return users.get(username);
        }

        return null;
    }

    public Manager getManager(String username) {
        if (managers.containsKey(username)) {
            return managers.get(username);
        }
        return null;
    }

    public Customer addUser(Customer customer, String path) {
        String customersPath = path + "\\data\\customers.csv";
        String usersPath = path + "\\data\\users.csv";
        if (users.containsKey(customer.getUsername())) {
            return null;
        }
        customer.setRole(RoleEnum.CUSTOMER);
        customer.setDeleted(false);
        users.put(customer.getUsername(), customer);
        writeCustomerToFile(customersPath, customer);
        writeCustomerToFile(usersPath, customer);
        return customer;
    }

    public Manager addManager(Manager manager, String path) {
        String managersPath = path + "\\data\\managers.csv";
        String usersPath = path + "\\data\\users.csv";
        if (users.containsKey(manager.getUsername())) {
            return null;
        }
        manager.setRole(RoleEnum.MANAGER);
        if(Objects.nonNull(manager.getChocolateFactory()) ) {
            manager.setChocolateFactory(manager.getChocolateFactory());
        } else {
            manager.setChocolateFactory(null);
        }

        manager.setDeleted(false);

        managers.put(manager.getUsername(), manager);
        writeManagerToFile(managersPath, manager);
        writeUserToFile(usersPath, manager);
        return manager;
    }

    public Worker addWorker(WorkerDTO worker, String path) {
        String workersPath = path + "\\data\\workers.csv";
        String usersPath = path + "\\data\\users.csv";
        if (workers.containsKey(worker.getUsername())) {
            return null;
        }
        Worker w = new Worker();
        w.setUsername(worker.getUsername());
        w.setPassword(worker.getPassword());
        w.setFirstName(worker.getFirstName());
        w.setLastName(worker.getLastName());
        w.setGender(worker.getGender());
        w.setDate(worker.getDate());
        w.setRole(RoleEnum.WORKER);
        w.setDeleted(false);

        users.put(worker.getUsername(), w);
        writeUserToFile(usersPath, w);

        ChocolateFactoryDAO dao = new ChocolateFactoryDAO(path);
        ChocolateFactory factory = dao.getByName(worker.getFactoryName());
        w.setChocolateFactory(factory);

        workers.put(worker.getUsername(), w);
        writeWorkerToFile(workersPath, w);
        return w;
    }

    public void writeUserToFile(String path, User user) {
        try (Writer writer = new BufferedWriter(new FileWriter(path, true))) {
            writer.append(user.getUsername());
            writer.append(",");
            writer.append(user.getPassword());
            writer.append(",");
            writer.append(user.getFirstName());
            writer.append(",");
            writer.append(user.getLastName());
            writer.append(",");
            writer.append(user.getGender());
            writer.append(",");
            writer.append(user.getDate());
            writer.append(",");
            writer.append(user.getRole().name());
            writer.append(",");
            writer.append(String.valueOf(user.getDeleted()));
            writer.append("\n");
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void writeManagerToFile(String path, Manager manager) {
        try (Writer writer = new BufferedWriter(new FileWriter(path, true))) {
            writer.append(manager.getUsername());
            writer.append(",");
            writer.append(manager.getPassword());
            writer.append(",");
            writer.append(manager.getFirstName());
            writer.append(",");
            writer.append(manager.getLastName());
            writer.append(",");
            writer.append(manager.getGender());
            writer.append(",");
            writer.append(manager.getDate());
            writer.append(",");
            writer.append(manager.getRole().name());
            writer.append(",");
            writer.append(String.valueOf(manager.getDeleted()));
            writer.append(",");
            if (manager.getChocolateFactory() != null) {
                writer.append(manager.getChocolateFactory().getName());
            } else {
                writer.append("\"\"");
            }
            writer.append("\n");
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void writeWorkerToFile(String path, Worker worker) {
        try (Writer writer = new BufferedWriter(new FileWriter(path, true))) {
            writer.append(worker.getUsername());
            writer.append(",");
            writer.append(worker.getPassword());
            writer.append(",");
            writer.append(worker.getFirstName());
            writer.append(",");
            writer.append(worker.getLastName());
            writer.append(",");
            writer.append(worker.getGender());
            writer.append(",");
            writer.append(worker.getDate());
            writer.append(",");
            writer.append(worker.getRole().name());
            writer.append(",");
            writer.append(String.valueOf(worker.getDeleted()));
            writer.append(",");
            if (worker.getChocolateFactory() != null) {
                writer.append(worker.getChocolateFactory().getName());
            } else {
                writer.append("\"\"");
            }
            writer.append("\n");
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void writeCustomerToFile(String path, Customer customer) {
        try (Writer writer = new BufferedWriter(new FileWriter(path, true))) {
            writer.append(customer.getFirstName());
            writer.append(",");
            writer.append(customer.getLastName());
            writer.append(",");
            writer.append(customer.getUsername());
            writer.append(",");
            writer.append(customer.getPassword());
            writer.append(",");
            writer.append(customer.getGender());
            writer.append(",");
            writer.append(customer.getDate());
            writer.append(",");
            writer.append(customer.getRole().name());
            writer.append(",");
            writer.append(NullToString(customer.getPoints()));
            writer.append(",");
            writer.append(getCustomerTypeToString(customer.getType()));
            writer.append(",");
            writer.append(String.valueOf(customer.getDeleted()));
            writer.append("\n");
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String getCustomerTypeToString(CustomerType status) {
        if (status != null) if (status.getType() == TypeEnum.BRONZE) {
            return "BRONZE";
        } else if (status.getType() == TypeEnum.SILVER) {
            return "SILVER";
        } else if (status.getType() == TypeEnum.GOLD) {
            return "GOLD";
        }
        return "";
    }

    public List<User> getAllUsers() {
        List<User> returnUsers = new ArrayList<>();
        for (User user : users.values()) {
            if (!user.getDeleted()) {
                returnUsers.add(user);
            }
        }
        returnUsers.add(admininstrator);
        return returnUsers;
    }

    public List<Manager> getAllManagers() {
        List<Manager> returnManagers = new ArrayList<>();
        for (Manager manager : managers.values()) {
            if (!manager.getDeleted()) {
                returnManagers.add(manager);
            }
        }
        return returnManagers;
    }

    public List<Worker> getAllWorkers() {
        List<Worker> returnWorkers = new ArrayList<>();
        for (Worker worker : workers.values()) {
            if (!worker.getDeleted()) {
                returnWorkers.add(worker);
            }
        }
        return returnWorkers;
    }

    public Customer getCustomer(String username) {
        if (customers.containsKey(username)) {
            return customers.get(username);
        }
        return null;
    }

    public Worker getWorker(String username) {
        if (workers.containsKey(username)) {
            return workers.get(username);
        }
        return null;
    }

    public User updateUser(User user, String path) {
        String usersPath = path + "\\data\\users.csv";
        String customersPath = path + "\\data\\customers.csv";
        String managersPath = path + "\\data\\managers.csv";
        String workersPath = path + "\\data\\workers.csv";
        User u = getByUsername(user.getUsername());
        Manager m = getManager(user.getUsername());
        Worker w = getWorker(user.getUsername());
        Customer c = getCustomer(user.getUsername());

        if(Objects.nonNull(m)) {
            writeManagersToFile(workersPath);
        }
        if(Objects.nonNull(w)) {
            writeWorkersToFile(managersPath);
        }
        if(Objects.nonNull(c)) {
            writeCustomersToFile(customersPath);
        }

        writeUsersToFile(usersPath);

        return user;
    }

    private void writeUsersToFile(String usersPath) {
        try (Writer writer = new BufferedWriter(new FileWriter(usersPath))) {
            for(User user : users.values()) {
                writer.append(user.getUsername());
                writer.append(",");
                writer.append(user.getPassword());
                writer.append(",");
                writer.append(user.getFirstName());
                writer.append(",");
                writer.append(user.getLastName());
                writer.append(",");
                writer.append(user.getGender());
                writer.append(",");
                writer.append(user.getDate());
                writer.append(",");
                writer.append(user.getRole().name());
                writer.append(",");
                writer.append(String.valueOf(user.getDeleted()));
            }
            writer.append("\n");
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    private void writeManagersToFile(String usersPath) {
        try (Writer writer = new BufferedWriter(new FileWriter(usersPath))) {
            for(Manager manager : managers.values()) {
                writer.append(manager.getUsername());
                writer.append(",");
                writer.append(manager.getPassword());
                writer.append(",");
                writer.append(manager.getFirstName());
                writer.append(",");
                writer.append(manager.getLastName());
                writer.append(",");
                writer.append(manager.getGender());
                writer.append(",");
                writer.append(manager.getDate());
                writer.append(",");
                writer.append(manager.getRole().name());
                writer.append(",");
                writer.append(String.valueOf(manager.getDeleted()));
            }
            writer.append("\n");
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    private void writeWorkersToFile(String usersPath) {
        try (Writer writer = new BufferedWriter(new FileWriter(usersPath))) {
            for(Worker worker : workers.values()) {
                writer.append(worker.getUsername());
                writer.append(",");
                writer.append(worker.getPassword());
                writer.append(",");
                writer.append(worker.getFirstName());
                writer.append(",");
                writer.append(worker.getLastName());
                writer.append(",");
                writer.append(worker.getGender());
                writer.append(",");
                writer.append(worker.getDate());
                writer.append(",");
                writer.append(worker.getRole().name());
                writer.append(",");
                writer.append(String.valueOf(worker.getDeleted()));
                writer.append(",");
                if (worker.getChocolateFactory() != null) {
                    writer.append(worker.getChocolateFactory().getName());
                } else {
                    writer.append("\"\"");
                }
            }
            writer.append("\n");
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    private void writeCustomersToFile(String usersPath) {
        try (Writer writer = new BufferedWriter(new FileWriter(usersPath))) {
            for(Customer customer : customers.values()) {
                writer.append(customer.getFirstName());
                writer.append(",");
                writer.append(customer.getLastName());
                writer.append(",");
                writer.append(customer.getUsername());
                writer.append(",");
                writer.append(customer.getPassword());
                writer.append(",");
                writer.append(customer.getGender());
                writer.append(",");
                writer.append(customer.getDate());
                writer.append(",");
                writer.append(customer.getRole().name());
                writer.append(",");
                writer.append(NullToString(customer.getPoints()));
                writer.append(",");
                writer.append(getCustomerTypeToString(customer.getType()));
                writer.append(",");
                writer.append(String.valueOf(customer.getDeleted()));
            }
            writer.append("\n");
            writer.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
