package beans;

import java.util.List;

public class Purchases {

    private List<Chocolate> chocolates;
    private User user;
    private double totalPrice;

    public Purchases() {
    }

    public Purchases(List<Chocolate> chocolates, User user, double totalPrice) {
        this.chocolates = chocolates;
        this.user = user;
        this.totalPrice = totalPrice;
    }

    public List<Chocolate> getChocolates() {
        return chocolates;
    }

    public void setChocolates(List<Chocolate> chocolates) {
        this.chocolates = chocolates;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    // Method to calculate total price of chocolates in the cart
    public void calculateTotalPrice() {
        this.totalPrice = chocolates.stream()
                .mapToDouble(chocolate -> chocolate.getPrice() * chocolate.getQuantity())
                .sum();
    }

    // toString method for displaying the object's data
    @Override
    public String toString() {
        return "Cart{" +
                "chocolates=" + chocolates +
                ", user='" + user + '\'' +
                ", totalPrice=" + totalPrice +
                '}';
    }
}