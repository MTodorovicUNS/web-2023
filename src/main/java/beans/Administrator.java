package beans;

import enums.RoleEnum;

public class Administrator extends User {

    public Administrator() {
        super();
    }

    public Administrator(String username, String password, String firstName, String lastName, String gender, String date, RoleEnum role, String jwt,
                         Boolean deleted) {
        super(username, password, firstName, lastName, gender, date, role, jwt, deleted);
    }
}
