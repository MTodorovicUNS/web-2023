package beans;
public class Chocolate {

    private String name;
    private double price;
    private String type;
    private ChocolateFactory factory;
    private String flavor;
    private float weight;
    private String description;
    private String image;
    private boolean inStock; // In stock, Out of stock
    private int quantity;

    // Constructor
    public Chocolate(String name, double price, String type, ChocolateFactory factory, String flavor, float weight, String description, String image, boolean inStock, int quantity) {
        this.name = name;
        this.price = price;
        this.type = type;
        this.factory = factory;
        this.flavor = flavor;
        this.weight = weight;
        this.description = description;
        this.image = image;
        this.inStock = inStock;
        this.quantity = quantity;
    }

    public Chocolate() {

    }

    // Getters and Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ChocolateFactory getFactory() {
        return factory;
    }

    public void setFactory(ChocolateFactory factory) {
        this.factory = factory;
    }

    public String getFlavor() {
        return flavor;
    }

    public void setFlavor(String flavor) {
        this.flavor = flavor;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public float getWeight() {
        return weight;
    }

    public boolean isInStock() {
        return inStock;
    }

    public void setInStock(boolean inStock) {
        this.inStock = inStock;
    }
}