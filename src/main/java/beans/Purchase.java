package beans;

import enums.PurchaseStatus;

import java.time.LocalDateTime;
import java.util.List;

public class Purchase {

    private String id;
    private List<Chocolate> chocolates;
    private ChocolateFactory factory;
    private LocalDateTime purchaseDateTime;
    private double totalPrice;
    private Customer customer;
    private PurchaseStatus status; // Processing, Approved, Rejected, Canceled

    // Constructor
    public Purchase(String id, List<Chocolate> chocolates, ChocolateFactory factory, LocalDateTime purchaseDateTime, double totalPrice, Customer customer, PurchaseStatus status) {
        this.id = id;
        this.chocolates = chocolates;
        this.factory = factory;
        this.purchaseDateTime = purchaseDateTime;
        this.totalPrice = totalPrice;
        this.customer = customer;
        this.status = status;
    }

    // Getters and Setters
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Chocolate> getChocolates() {
        return chocolates;
    }

    public void setChocolates(List<Chocolate> chocolates) {
        this.chocolates = chocolates;
    }

    public ChocolateFactory getFactory() {
        return factory;
    }

    public void setFactory(ChocolateFactory factory) {
        this.factory = factory;
    }

    public LocalDateTime getPurchaseDateTime() {
        return purchaseDateTime;
    }

    public void setPurchaseDateTime(LocalDateTime purchaseDateTime) {
        this.purchaseDateTime = purchaseDateTime;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public PurchaseStatus getStatus() {
        return status;
    }

    public void setStatus(PurchaseStatus status) {
        this.status = status;
    }

    // toString method for displaying the object's data
    @Override
    public String toString() {
        return "Purchase{" +
                "id='" + id + '\'' +
                ", chocolates=" + chocolates +
                ", factory='" + factory + '\'' +
                ", purchaseDateTime=" + purchaseDateTime +
                ", totalPrice=" + totalPrice +
                ", customer='" + customer + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
