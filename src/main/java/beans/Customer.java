package beans;

import enums.RoleEnum;
import enums.TypeEnum;

import java.util.List;

public class Customer extends User {

    private List<ChocolateFactory> chocolateFactories;
    private int points;
    private CustomerType type;

    public Customer() {
        super();
    }

    public Customer(String username, String password, String firstName, String lastName, String gender, String date, int points, CustomerType type,
					Boolean deleted) {
        super(username, password, firstName, lastName, gender, date, RoleEnum.CUSTOMER, deleted);
        this.points = points;
        this.type = type;
    }

    public Customer(List<ChocolateFactory> chocolateFactories, int points, CustomerType type) {
        super();
        this.chocolateFactories = chocolateFactories;
        this.points = points;
        this.type = type;
    }

    public Customer(String username, String password, String firstName, String lastName, String gender, String date, RoleEnum role, String jwt, boolean deleted, int points, CustomerType type) {
        super(username, password, firstName, lastName, gender, date, role, jwt, deleted);
        this.points = points;
        this.type = type;
    }

    public Customer(String username, String password, String firstName, String lastName, String gender, String date, RoleEnum role, boolean deleted, int points, CustomerType type) {
        super(username, password, firstName, lastName, gender, date, role, deleted);
        this.points = points;
        this.type = type;
    }

    public CustomerType getType() {
        return type;
    }

    public void setType(CustomerType type) {
        this.type = type;
    }

    public List<ChocolateFactory> getChocolateFactories() {
        return chocolateFactories;
    }

    public void setChocolateFactories(List<ChocolateFactory> chocolateFactories) {
        this.chocolateFactories = chocolateFactories;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }


}
