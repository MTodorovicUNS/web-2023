package beans;

import enums.RoleEnum;

public class Worker extends User {
    private ChocolateFactory chocolateFactory;
    public Worker() {
    }

    public Worker(ChocolateFactory chocolateFactory) {
        this.chocolateFactory = chocolateFactory;
    }

    public Worker(String username, String password, String firstName, String lastName, String gender, String date, RoleEnum role, String jwt, boolean deleted, ChocolateFactory chocolateFactory) {
        super(username, password, firstName, lastName, gender, date, role, jwt, deleted);
        this.chocolateFactory = chocolateFactory;
    }

    public Worker(String username, String password, String firstName, String lastName, String gender, String date, RoleEnum role, boolean deleted, ChocolateFactory chocolateFactory) {
        super(username, password, firstName, lastName, gender, date, role, deleted);
        this.chocolateFactory = chocolateFactory;
    }

    public ChocolateFactory getChocolateFactory() {
        return chocolateFactory;
    }

    public void setChocolateFactory(ChocolateFactory chocolateFactory) {
        this.chocolateFactory = chocolateFactory;
    }
}
