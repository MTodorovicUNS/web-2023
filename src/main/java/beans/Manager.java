package beans;

import enums.RoleEnum;

public class Manager extends User {
	private ChocolateFactory chocolateFactory;

	public Manager() {
	}

	public Manager(ChocolateFactory chocolateFactory) {
		this.chocolateFactory = chocolateFactory;
	}

	public Manager(String username, String password, String firstName, String lastName, String gender, String date, RoleEnum role, String jwt, boolean deleted, ChocolateFactory chocolateFactory) {
		super(username, password, firstName, lastName, gender, date, role, jwt, deleted);
		this.chocolateFactory = chocolateFactory;
	}

	public Manager(String username, String password, String firstName, String lastName, String gender, String date, RoleEnum role, boolean deleted, ChocolateFactory chocolateFactory) {
		super(username, password, firstName, lastName, gender, date, role, deleted);
		this.chocolateFactory = chocolateFactory;
	}

	public ChocolateFactory getChocolateFactory() {
		return chocolateFactory;
	}

	public void setChocolateFactory(ChocolateFactory chocolateFactory) {
		this.chocolateFactory = chocolateFactory;
	}
}
