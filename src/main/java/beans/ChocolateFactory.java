package beans;

import java.util.List;

public class ChocolateFactory {
    private String name;
    private List<Chocolate> chocolates;
    private String workingHours;
    private boolean isWorking;
    private Location location;
    private String logo;
    private Float review;

    public ChocolateFactory() {
    }

    public ChocolateFactory(String name, List<Chocolate> chocolates, String workingHours, boolean isWorking, Location location, String logo,
                            Float review) {
        this.name = name;
        this.chocolates = chocolates;
        this.workingHours = workingHours;
        this.isWorking = isWorking;
        this.location = location;
        this.logo = logo;
        this.review = review;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Chocolate> getChocolates() {
        return chocolates;
    }

    public void setChocolates(List<Chocolate> chocolates) {
        this.chocolates = chocolates;
    }

    public String getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(String workingHours) {
        this.workingHours = workingHours;
    }

    public Boolean getIsWorking() {
        return isWorking;
    }

    public void setIsWorking(Boolean isWorking) {
        this.isWorking = isWorking;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Float getReview() {
        return review;
    }

    public void setReview(Float review) {
        this.review = review;
    }
}
