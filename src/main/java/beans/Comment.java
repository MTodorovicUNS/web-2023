package beans;

public class Comment {
    private String id;
    private Customer customer;
    private ChocolateFactory chocolateFactory;
    private String text;
    private Float rate;
    private Boolean accepted;

    public Comment() {
    }

    public Comment(String id, Customer customer, ChocolateFactory chocolateFactory, String text, Float rate, Boolean accepted) {
        this.id = id;
        this.customer = customer;
        this.chocolateFactory = chocolateFactory;
        this.text = text;
        this.rate = rate;
        this.accepted = accepted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public ChocolateFactory getChocolateFactory() {
        return chocolateFactory;
    }

    public void setChocolateFactory(ChocolateFactory chocolateFactory) {
        this.chocolateFactory = chocolateFactory;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Float getRate() {
        return rate;
    }

    public void setRate(Float rate) {
        this.rate = rate;
    }

    public Boolean getAccepted() {
        return accepted;
    }

    public void setAccepted(Boolean accepted) {
        this.accepted = accepted;
    }
}
