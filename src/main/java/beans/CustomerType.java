package beans;

import enums.TypeEnum;

public class CustomerType {

    private TypeEnum type;
    private float discount;
    private int points;

    public CustomerType() {
    }

    public CustomerType(TypeEnum type, float discount, int points) {
        this.type = type;
        this.discount = discount;
        this.points = points;
    }

    public TypeEnum getType() {
        return type;
    }

    public void setType(TypeEnum type) {
        this.type = type;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
