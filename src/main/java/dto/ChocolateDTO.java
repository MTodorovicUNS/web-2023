package dto;

public class ChocolateDTO {
    private String name;
    private double price;
    private String type;
    private String factoryName;
    private String flavor;
    private float weight;
    private String description;
    private String image;
    private boolean inStock; // In stock, Out of stock
    private int quantity;

    public ChocolateDTO() {
    }

    public ChocolateDTO(String name, double price, String type, String factoryName, String flavor, float weight, String description, String image,
                        boolean inStock, int quantity) {
        this.name = name;
        this.price = price;
        this.type = type;
        this.factoryName = factoryName;
        this.flavor = flavor;
        this.weight = weight;
        this.description = description;
        this.image = image;
        this.inStock = inStock;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFactoryName() {
        return factoryName;
    }

    public void setFactoryName(String factoryName) {
        this.factoryName = factoryName;
    }

    public String getFlavor() {
        return flavor;
    }

    public void setFlavor(String flavor) {
        this.flavor = flavor;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isInStock() {
        return inStock;
    }

    public void setInStock(boolean inStock) {
        this.inStock = inStock;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
