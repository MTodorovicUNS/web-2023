package dto;

import beans.Chocolate;
import beans.Comment;
import beans.Location;
import beans.Manager;

import java.util.List;

public class ChocolateFactoryReturnDTO {
    private String name;
    private List<Chocolate> chocolates;
    private String workingHours;
    private boolean isWorking;
    private Location location;
    private String logo;
    private Float review;
    private Manager manager;
    private List<Comment> comments;

    public ChocolateFactoryReturnDTO() {
    }

    public ChocolateFactoryReturnDTO(String name, List<Chocolate> chocolates, String workingHours, boolean isWorking, Location location, String logo, Float review, Manager manager) {
        this.name = name;
        this.chocolates = chocolates;
        this.workingHours = workingHours;
        this.isWorking = isWorking;
        this.location = location;
        this.logo = logo;
        this.review = review;
        this.manager = manager;
    }

    public ChocolateFactoryReturnDTO(String name, List<Chocolate> chocolates, String workingHours, boolean isWorking, Location location, String logo, Float review, Manager manager, List<Comment> comments) {
        this.name = name;
        this.chocolates = chocolates;
        this.workingHours = workingHours;
        this.isWorking = isWorking;
        this.location = location;
        this.logo = logo;
        this.review = review;
        this.manager = manager;
        this.comments = comments;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Chocolate> getChocolates() {
        return chocolates;
    }

    public void setChocolates(List<Chocolate> chocolates) {
        this.chocolates = chocolates;
    }

    public String getWorkingHours() {
        return workingHours;
    }

    public void setWorkingHours(String workingHours) {
        this.workingHours = workingHours;
    }

    public boolean getIsWorking() {
        return isWorking;
    }

    public void setIsWorking(boolean isWorking) {
        this.isWorking = isWorking;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public Float getReview() {
        return review;
    }

    public void setReview(Float review) {
        this.review = review;
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }
}
