package enums;

public enum PurchaseStatus {
    PROCESSING, APPROVED, REJECTED, CANCELLED
}
