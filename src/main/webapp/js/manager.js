const getAllChocolatesURL = 'http://localhost:8080/web_2024/rest/chocolate/all';
const getOneChocolateURL = 'http://localhost:8080/web_2024/rest/chocolate/name';
const addChocolateUrl = 'http://localhost:8080/web_2024/rest/chocolate/add';
const editChocolateUrl = 'http://localhost:8080/web_2024/rest/chocolate/edit';
const deleteChocolateUrl = 'http://localhost:8080/web_2024/rest/chocolate/delete';
const addWorkerUrl = 'http://localhost:8080/web_2024/rest/user/worker/add';
const getAllWorkersUrl = 'http://localhost:8080/web_2024/rest/user/worker/all';
let user = JSON.parse(localStorage.getItem('user'));

// Chocolate Modal
const chocolateModal = document.getElementById("chocolateModal");
const editChocolateModal = document.getElementById("editChocolateModal");
const openChocolateModalBtn = document.getElementById("openChocolateModalBtn");
const editCloseChocolateModal = document.getElementById("editCloseChocolateModal");
const closeChocolateModal = document.getElementById("closeChocolateModal");

openChocolateModalBtn.onclick = function () {
    chocolateModal.style.display = "block";
}

closeChocolateModal.onclick = function () {
    chocolateModal.style.display = "none";
}

editCloseChocolateModal.onclick = function () {
    editChocolateModal.style.display = "none";
}

window.onclick = function (event) {
    if (event.target == chocolateModal) {
        chocolateModal.style.display = "none";
    }
}

// Worker Modal
const workerModal = document.getElementById("workerModal");
const openWorkerModalBtn = document.getElementById("openWorkerModalBtn");
const closeWorkerModal = document.getElementById("closeWorkerModal");

openWorkerModalBtn.onclick = function () {
    workerModal.style.display = "block";
}

closeWorkerModal.onclick = function () {
    workerModal.style.display = "none";
}

window.onclick = function (event) {
    if (event.target == workerModal) {
        workerModal.style.display = "none";
    }
}

function addEventListenersToButtons() {
    // Edit button functionality
    console.log(document.querySelectorAll(".edit-btn"));
    document.querySelectorAll(".edit-btn").forEach(button => {
        button.addEventListener("click", function () {
            const name = this.getAttribute("data-name");
            fetchChocolateDetails(name, user.chocolateFactory.name);
            editChocolateModal.style.display = "block";
        });
    });
    // Delete button functionality
    document.querySelectorAll(".delete-btn").forEach(button => {
        button.addEventListener("click", function () {
            const factoryName = this.getAttribute("data-name");
            if (confirm("Are you sure you want to delete this factory?")) {
                deleteFactory(factoryName);
            }
        });
    });
}

// Add Chocolate
document.getElementById('addChocolateBtn').addEventListener('click', function () {
    // Get form field values
    const chocolateName = document.getElementById('chocolateName').value;
    const chocolatePrice = document.getElementById('chocolatePrice').value;
    const chocolateType = document.getElementById('chocolateType').value;
    const chocolateFlavor = document.getElementById('chocolateFlavor').value;
    const chocolateWeight = document.getElementById('chocolateWeight').value;
    const chocolateDescription = document.getElementById('chocolateDescription').value;
    const chocolateImage = document.getElementById('chocolateImage').value;
    const chocolateInStock = document.getElementById('chocolateInStock').value;
    const chocolateQuantity = document.getElementById('chocolateQuantity').value;

    // Create chocolate object to send
    const chocolateData = {
        name: chocolateName,
        price: parseFloat(chocolatePrice),
        type: chocolateType,
        flavor: chocolateFlavor,
        weight: parseFloat(chocolateWeight),
        description: chocolateDescription,
        image: chocolateImage,
        inStock: chocolateInStock === 'true',
        quantity: parseInt(chocolateQuantity),
        factoryName: user.chocolateFactory.name
    };

    // Send the data using a POST request
    fetch(addChocolateUrl, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(chocolateData)
    })
        .then(response => response.json())
        .then(data => {
            if (data.success) {
                alert('Chocolate added successfully!');

                // Optionally reload or redirect the page
            } else {
                alert('Error adding chocolate: ' + data.message);
            }
            chocolateModal.style.display = "none";
        })
        .catch(error => {
            console.error('Error:', error);
            alert('Failed to add chocolate');
        });
});
const chocolateTable = document.querySelector('#chocolatesTable tbody');

// Load Chocolates
function loadChocolates() {
    fetch(getAllChocolatesURL + "?factoryName=" + user.chocolateFactory.name)
        .then(response => response.json())
        .then(chocolates => {
            chocolateTable.innerHTML = '';

            chocolates.forEach(chocolate => {
                const row = document.createElement('tr');

                row.innerHTML = `
                    <td>${chocolate.name}</td>
                    <td>${chocolate.price}</td>
                    <td>${chocolate.type}</td>
                    <td>${chocolate.flavor}</td>
                    <td>${chocolate.weight}</td>
                    <td>${chocolate.description}</td>
                    <td><img src="${chocolate.image}"/></td>
                    <td>${chocolate.inStock}</td>
                    <td>${chocolate.quantity}</td>
                    <td class="actions-btn">
                        <button class="edit-btn" data-name="${chocolate.name}">Edit</button>
                        <button class="delete-btn" data-name="${chocolate.name}">Delete</button>
                    </td>
                `;

                chocolateTable.appendChild(row);
            });
            addEventListenersToButtons();

        });
}

// Edit Chocolate
document.getElementById('editChocolateBtn').addEventListener('click', function () {
    const chocolateName = document.getElementById('editChocolateName').value;
    const chocolatePrice = document.getElementById('editChocolatePrice').value;
    const chocolateType = document.getElementById('editChocolateType').value;
    const chocolateFlavor = document.getElementById('editChocolateFlavor').value;
    const chocolateWeight = document.getElementById('editChocolateWeight').value;
    const chocolateDescription = document.getElementById('editChocolateDescription').value;
    const chocolateImage = document.getElementById('editChocolateImage').value;
    const chocolateInStock = document.getElementById('editChocolateInStock').value;
    const chocolateQuantity = document.getElementById('editChocolateQuantity').value;

    // Create chocolate object to send
    const chocolateData = {
        name: chocolateName,
        price: parseFloat(chocolatePrice),
        type: chocolateType,
        flavor: chocolateFlavor,
        weight: parseFloat(chocolateWeight),
        description: chocolateDescription,
        image: chocolateImage,
        inStock: chocolateInStock === 'true',
        quantity: parseInt(chocolateQuantity),
        factoryName: user.chocolateFactory.name
    };

    // Send the data using a POST request
    fetch(editChocolateUrl, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(chocolateData)
    })
        .then(response => response.json())
        .then(data => {
            if (data.success) {
                alert('Chocolate added successfully!');

                // Optionally reload or redirect the page
            } else {
                alert('Error adding chocolate: ' + data.message);
            }
            chocolateModal.style.display = "none";
        })
        .catch(error => {
            console.error('Error:', error);
            alert('Failed to add chocolate');
        });
});

// Delete Chocolate
function deleteChocolate(id) {
    if (confirm('Are you sure you want to delete this chocolate?')) {
        fetch(`${deleteChocolateUrl}/${id}`, {
            method: 'DELETE',
        })
            .then(response => response.json())
            .then(data => {
                alert('Chocolate deleted successfully!');
                loadChocolates();
            })
            .catch(error => console.error('Error deleting chocolate:', error));
    }
}

document.getElementById('addWorkerBtn').addEventListener('click', function (event) {
    event.preventDefault();

    let username = document.getElementById('username').value;
    let password = document.getElementById('password').value;
    let firstName = document.getElementById('first_name').value;
    let lastName = document.getElementById('last_name').value;
    let gender = document.getElementById('gender').value;
    let date = document.getElementById('birth_date').value;
    let validate = false;

    var regName = new RegExp("([A-Z]{1})([a-z]+)$");
    var regUser = new RegExp("[a-zA-Z0-9]+$");

    // First name validation
    if (firstName === "") {
        document.getElementById("eri").removeAttribute("hidden");
    } else {
        document.getElementById("eri").setAttribute("hidden", "true");
        validate = true;
    }

    // Last name validation
    if (lastName === "") {
        document.getElementById("erp").removeAttribute("hidden");
        validate = false;
    } else {
        document.getElementById("erp").setAttribute("hidden", "true");
        if (validate) validate = true;
    }

    // Date of birth validation
    if (date === "") {
        document.getElementById("erd").removeAttribute("hidden");
        validate = false;
    } else {
        document.getElementById("erd").setAttribute("hidden", "true");
        if (validate) validate = true;
    }

    // Username validation
    if (!regUser.test(username)) {
        document.getElementById("erk").removeAttribute("hidden");
        validate = false;
    } else {
        document.getElementById("erk").setAttribute("hidden", "true");
        if (validate) validate = true;
    }

    // Password validation
    if (!regUser.test(password)) {
        document.getElementById("erl").removeAttribute("hidden");
        validate = false;
    } else {
        document.getElementById("erl").setAttribute("hidden", "true");
        if (validate) validate = true;
    }
    let user = JSON.parse(localStorage.getItem('user'));

    // If all validations pass, proceed with the registration
    if (validate) {
        let data = JSON.stringify({
            username: username,
            password: password,
            firstName: firstName,
            lastName: lastName,
            gender: gender,
            date: date,
            factoryName: user.chocolateFactory.name
        });

        fetch(addWorkerUrl, {
            method: 'POST', headers: {
                'Content-Type': 'application/json'
            }, body: data
        })
            .then(response => response.json())
            .then(data => {
                alert('Worker added successfully!');
                workerModal.style.display = "none";
                loadWorkers();
            })
            .catch(error => console.error('Error adding worker:', error));
    }
});

const workersTableBody = document.querySelector('#workersTable tbody');

function loadWorkers() {
    let user = JSON.parse(localStorage.getItem('user'));

    fetch(getAllWorkersUrl + '?factoryName=' + user.chocolateFactory.name)
        .then(response => response.json())
        .then(workers => {
            workersTableBody.innerHTML = '';

            workers.forEach(worker => {
                const row = document.createElement('tr');

                row.innerHTML = `
                    <td>${worker.username}</td>
                    <td>${worker.firstName}</td>
                    <td>${worker.lastName}</td>
                    <td>${worker.gender}</td>
                    <td>${worker.date}</td>
                    <td>${worker.role}</td>
                    <td class="actions-btn">
                        <button class="edit-btn" data-name="${worker.name}">Edit</button>
                        <button class="delete-btn" data-name="${worker.name}">Delete</button>
                    </td>
                `;

                workersTableBody.appendChild(row);
            });

        });
}


function fetchChocolateDetails(name, factoryName) {

    fetch(`http://localhost:8080/web_2024/rest/chocolate/name?name=${name}&factoryName=${encodeURIComponent(factoryName)}`)
        .then(response => response.json())
        .then(chocolate => {
            if (chocolate) {
                displayChocolateDetails(chocolate);
            } else {
                alert('chocolate not found!');
            }
        })
        .catch(error => {
            console.error('Error fetching chocolate:', error);
        });
}

function displayChocolateDetails(chocolate) {
    console.log(chocolate)
    document.getElementById('editChocolateName').setAttribute('value', chocolate.name);
    document.getElementById('editChocolatePrice').setAttribute('value', chocolate.price);
    document.getElementById('editChocolateType').setAttribute('value', chocolate.type);
    document.getElementById('editChocolateFlavor').setAttribute('value', chocolate.flavor);
    document.getElementById('editChocolateWeight').setAttribute('value', chocolate.weight);
    document.getElementById('editChocolateDescription').setAttribute('value', chocolate.description);
    document.getElementById('editChocolateImage').setAttribute('value', chocolate.image);
    document.getElementById('editChocolateQuantity').setAttribute('value', chocolate.quantity);
    document.getElementById('editChocolateInStock').setAttribute('value', chocolate.inStock);
}


document.addEventListener('DOMContentLoaded', function () {
    loadChocolates();
    loadWorkers();
}, false);
