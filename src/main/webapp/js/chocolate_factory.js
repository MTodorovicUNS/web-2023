// Function to get the factory name from the URL query parameters
function getFactoryNameFromURL() {
    const params = new URLSearchParams(window.location.search);
    return params.get('name');  // Assuming the query parameter is ?name=<factory-name>
}

// Function to fetch the factory data from the API using the factory name
function fetchFactoryDetails(factoryName) {
    fetch(`http://localhost:8080/web_2024/rest/chocolate/factory/name?name=${encodeURIComponent(factoryName)}`)
        .then(response => response.json())
        .then(factory => {
            if (factory) {
                displayFactoryDetails(factory);
            } else {
                alert('Factory not found!');
            }
        })
        .catch(error => {
            console.error('Error fetching factory:', error);
        });
}

// Function to display the factory details on the page
function displayFactoryDetails(factory) {

    document.getElementById('factoryName').textContent = factory.name;
    document.getElementById('workingHours').textContent = factory.workingHours;
    document.getElementById('isWorking').textContent = factory.isWorking;
    document.getElementById('rating').textContent = factory.rating;
    document.getElementById('logo').setAttribute('src', factory.logo);
    let chocolates = document.getElementById('chocolates');
    let comments = document.getElementById('comments');
    factory.chocolates.forEach(chocolate => {
        const chocolateDiv = document.createElement('div');
        chocolateDiv.innerHTML = `
            <p>Name: ${chocolate.name}</p>
            <p>Quantity: ${chocolate.quantity}</p>
        `;
        chocolates.appendChild(chocolateDiv);
    })
    factory.comments.forEach(comment => {
        const chocolateDiv = document.createElement('div');
        chocolateDiv.innerHTML = `
            <p>Text: ${comment.text}</p>
            <p>Rating: ${comment.rate}</p>
            <p>Customer: ${comment.customer.firstName} ${comment.customer.lastName}</p>
        `;
        comments.appendChild(chocolateDiv);
    })
}

// Function to handle deleting a factory
function deleteFactory(factoryName) {
    if (confirm('Are you sure you want to delete this factory?')) {
        fetch(`https://your-api-endpoint.com/factories/${encodeURIComponent(factoryName)}`, {
            method: 'DELETE',
        })
            .then(response => {
                if (response.ok) {
                    alert('Factory deleted successfully!');
                    window.location.href = 'admin_page.html';  // Redirect back to admin page after deletion
                } else {
                    alert('Failed to delete factory.');
                }
            })
            .catch(error => {
                console.error('Error deleting factory:', error);
            });
    }
}

// Function to handle editing a factory (redirect to edit page)
function editFactory(factoryName) {
    window.location.href = `edit_factory.html?name=${encodeURIComponent(factoryName)}`;
}

// Attach event listeners to buttons
document.addEventListener('DOMContentLoaded', () => {
    const factoryName = getFactoryNameFromURL();

    if (factoryName) {
        fetchFactoryDetails(factoryName);

        document.getElementById('editBtn').addEventListener('click', () => {
            editFactory(factoryName);
        });

        document.getElementById('deleteBtn').addEventListener('click', () => {
            deleteFactory(factoryName);
        });
    } else {
        alert('No factory name provided in the URL.');
    }
});
