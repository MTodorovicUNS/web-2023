document.addEventListener('DOMContentLoaded', () => {
    const userProfile = JSON.parse(localStorage.getItem('user'));

    if (userProfile) {
        document.getElementById('username').value = userProfile.username || '';
        document.getElementById('firstName').value = userProfile.firstName || '';
        document.getElementById('lastName').value = userProfile.lastName || '';
        document.getElementById('email').value = userProfile.email || '';
        document.getElementById('dateOfBirth').value = userProfile.dateOfBirth || '';
        document.getElementById('gender').value = userProfile.gender || '';
    } else {
        console.error('No user profile found in local storage.');
        window.location.href = 'error_page.html';
    }
});

const updateProfileBtn = document.getElementById('updateProfileBtn');

updateProfileBtn.addEventListener('click', (event) => {
    event.preventDefault();

    const username = document.getElementById('username').value;
    const password = document.getElementById('password').value;
    const firstName = document.getElementById('firstName').value;
    const lastName = document.getElementById('lastName').value;
    const gender = document.getElementById('gender').value;
    const dateOfBirth = document.getElementById('dateOfBirth').value;

    const userData = {
        username: username,
        password: password,
        firstName: firstName,
        lastName: lastName,
        gender: gender,
        date: dateOfBirth
    };

    fetch('http://localhost:8080/web_2024/rest/user/update', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(userData)
    })
        .then(response => {
            if (!response.ok) {
                return response.json().then(error => {
                    throw new Error('Error updating profile: ' + error.message);
                });
            }
            return response.json();
        })
        .then(result => {
            alert('Profile updated successfully!');
            console.log(result);
        })
        .catch(error => {
            console.error('Error:', error);
            alert('An error occurred while updating the profile.');
        });
});