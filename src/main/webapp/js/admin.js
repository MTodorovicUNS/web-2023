const getAllUsersUrl = "http://localhost:8080/web_2024/rest/user/available/managers";
const postChocolateFactories = "http://localhost:8080/web_2024/rest/chocolate/factory/add";
const getChocolateFactoryUrl = "http://localhost:8080/web_2024/rest/chocolate/factory/name";
const editChocolateFactoryUrl = "http://localhost:8080/web_2024/rest/chocolate/factory/edit";


var factoryModal = document.getElementById("factoryModal");
var openFactoryBtn = document.getElementById("openFactoryModal");
var closeFactoryBtn = document.getElementById("closeFactoryModal");
var managerModal = document.getElementById("managerModal");
var openManagerBtn = document.getElementById("openManagerModal");
var closeManagerBtn = document.getElementById("closeManagerModal");

var editFactoryModal = document.getElementById("editFactoryModal");
var closeEditFactoryModal = document.getElementById("closeEditFactoryModal");

// Fetch users and populate the select dropdown
function populateManagers() {
    // Make an API request to get users
    fetch(getAllUsersUrl)
        .then(response => {
            if (!response.ok) {
                throw new Error("Network response was not ok " + response.statusText);
            }
            return response.json(); // Parse the JSON response
        })
        .then(data => {
            const select = document.getElementById('manager');
            select.innerHTML = ''; // Clear existing options

            // Create a placeholder option
            const placeholderOption = document.createElement('option');
            placeholderOption.value = '';
            placeholderOption.textContent = 'Select a manager';
            placeholderOption.disabled = true;
            placeholderOption.selected = true;
            select.appendChild(placeholderOption);

            // Populate the select dropdown with users
            data.forEach(user => {
                const option = document.createElement('option');
                option.value = user.username; // Assuming user.id is the user ID
                option.textContent = `${user.firstName} ${user.lastName}`; // Assuming firstName and lastName are available
                select.appendChild(option);
            });
        })
        .catch(error => {
            console.error("There was a problem with the fetch operation:", error);
            const select = document.getElementById('users');
            select.innerHTML = '<option value="">Error loading users</option>';
        });
}

document.getElementById('addChocolateFactoryBtn').addEventListener('click', function () {
    // Get input values
    const factoryName = document.getElementById("factoryName").value;
    const workingHours = document.getElementById("workingHours").value;
    const location = document.getElementById("location").value;
    const isWorking = document.getElementById("isWorking").value;
    const logo = document.getElementById("logo").value;
    const managerName = document.getElementById("manager").value;

    // Validate the input data
    if (factoryName === "" || workingHours === "" || workingHours === "" || isWorking === "" || managerName === "") {
        alert("Please fill in all fields correctly.");
        return;
    }

    // Create the factory object to send
    const factoryData = {
        name: factoryName,
        workingHours: workingHours,
        location: location,
        isWorking: isWorking,
        review: 1,
        managerName: managerName,
        logo: logo
    };

    // Send a POST request to create the factory
    fetch(postChocolateFactories, {
        method: 'POST', headers: {
            'Content-Type': 'application/json',
        }, body: JSON.stringify(factoryData)
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            alert("Factory created successfully!");
            // Optionally, reload or update the page with the new factory info
            // location.reload();
            factoryModal.style.display = "none";
        })
        .catch(error => {
            console.error('There was an error creating the factory:', error);
            alert("Error creating factory, please try again.");
        });
});

document.getElementById('editChocolateFactoryBtn').addEventListener('click', function () {
    // Get input values
    const factoryName = document.getElementById("editFactoryName").value;
    const workingHours = document.getElementById("editWorkingHours").value;
    const location = document.getElementById("editLocation").value;
    const isWorking = document.getElementById("editIsWorking").value;
    const logo = document.getElementById("editLogo").value;
    const managerName = document.getElementById("editManager").value;

    // Validate the input data
    if (factoryName === "" || workingHours === "" || workingHours === "" || isWorking === "" || managerName === "") {
        alert("Please fill in all fields correctly.");
        return;
    }

    // Create the factory object to send
    const factoryData = {
        name: factoryName,
        workingHours: workingHours,
        location: location,
        isWorking: isWorking,
        review: 1,
        managerName: managerName,
        logo: logo
    };

    // Send a POST request to create the factory
    fetch(editChocolateFactoryUrl, {
        method: 'PUT', headers: {
            'Content-Type': 'application/json',
        }, body: JSON.stringify(factoryData)
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            alert("Factory created successfully!");
            // Optionally, reload or update the page with the new factory info
            // location.reload();
            editFactoryModal.style.display = "none";
        })
        .catch(error => {
            console.error('There was an error creating the factory:', error);
            alert("Error creating factory, please try again.");
        });
});

const factoriesTableBody = document.querySelector("#factoriesTable tbody");

// Function to fetch factories from the REST API
function fetchFactories() {
    fetch("http://localhost:8080/web_2024/rest/chocolate/factory/all")
        .then(response => response.json())
        .then(data => {
            populateFactoryTable(data);
        })
        .catch(error => {
            console.error("Error fetching factories:", error);
        });
}

// Function to populate the factory table
function populateFactoryTable(factories) {
    factoriesTableBody.innerHTML = ""; // Clear the table first

    factories.forEach(factory => {
        const row = document.createElement("tr");

        row.innerHTML = `
                <td>${factory.name}</td>
                <td>${factory.location.address}</td>
                <td>${factory.isWorking}</td>
                <td>${factory.review}</td>
                <td>
                    <button class="view-btn" data-name="${factory.name}">View</button>
                    <button class="edit-btn" data-name="${factory.name}">Edit</button>
                    <button class="delete-btn" data-name="${factory.name}">Delete</button>
                </td>
            `;

        factoriesTableBody.appendChild(row);
    });

    addEventListenersToButtons();
}

function addEventListenersToButtons() {
    // View button functionality
    document.querySelectorAll(".view-btn").forEach(button => {
        button.addEventListener("click", function () {
            const factoryName = this.getAttribute("data-name");
            window.location.href = `view_factory.html?name=${factoryName}`;
        });
    });

    // Edit button functionality
    document.querySelectorAll(".edit-btn").forEach(button => {
        button.addEventListener("click", function () {
            const factoryName = this.getAttribute("data-name");
            fetchFactoryDetails(factoryName);
            editFactoryModal.style.display = "block";
        });
    });

    // Delete button functionality
    document.querySelectorAll(".delete-btn").forEach(button => {
        button.addEventListener("click", function () {
            const factoryName = this.getAttribute("data-name");
            if (confirm("Are you sure you want to delete this factory?")) {
                deleteFactory(factoryName);
            }
        });
    });
}

// Function to delete a factory
function deleteFactory(factoryId) {
    fetch(baseUrl, {
        method: "DELETE"
    })
        .then(response => {
            if (response.ok) {
                alert("Factory deleted successfully!");
                fetchFactories(); // Refresh the table after deletion
            } else {
                alert("Failed to delete factory.");
            }
        })
        .catch(error => {
            console.error("Error deleting factory:", error);
        });
}
$(document).ready(function () {


    openFactoryBtn.onclick = function () {
        factoryModal.style.display = "block";
    }

    closeFactoryBtn.onclick = function () {
        factoryModal.style.display = "none";
    }

    closeEditFactoryModal.onclick = function () {
        editFactoryModal.style.display = "none";
    }

// Modal handling for Manager Modal


    openManagerBtn.onclick = function () {
        managerModal.style.display = "block";
    }

    closeManagerBtn.onclick = function () {
        managerModal.style.display = "none";
    }

// Close modal when clicking outside of it
    window.onclick = function (event) {
        if (event.target == factoryModal) {
            factoryModal.style.display = "none";
        }
        if (event.target == managerModal) {
            managerModal.style.display = "none";
        }
    }

    const apiEndpoint = "http://localhost:8080/web_2024/rest/user/all";

// Fetch users when the page is loaded
    fetchUsers();

    function fetchUsers() {
        $.ajax({
            url: apiEndpoint,
            type: 'GET',
            dataType: 'json',
            success: function (users) {
                console.log(users)
                populateUsersTable(users);
            },
            error: function (error) {
                console.error('Error fetching users:', error);
            }
        });
    }

    function populateUsersTable(users) {
        // Clear existing rows
        const usersTableBody = $('.users-table tbody');
        usersTableBody.empty();

        // Loop through the users and append them to the table
        users.forEach(function (user) {
            const userRow = `
                <tr>
                    <td>${user.username}</td>
                    <td>${user.firstName}</td>
                    <td>${user.lastName}</td>
                    <td>${user.gender}</td>
                    <td>${user.date}</td>
                    <td>${user.role}</td>
                    <td>
                        <button class="btn-action" onclick="editUser('${user.username}')">Edit</button>
                        <button class="btn-action delete" onclick="deleteUser('${user.username}')">Delete</button>
                    </td>
                </tr>
            `;
            usersTableBody.append(userRow);
        });
    }

// Edit user functionality
    window.editUser = function (username) {
        // Code to edit the user can go here
        alert('Edit user: ' + username);
    }

// Delete user functionality
    window.deleteUser = function (username) {
        if (confirm('Are you sure you want to delete user: ' + username + '?')) {
            $.ajax({
                url: apiEndpoint + '/' + username,
                type: 'DELETE',
                success: function () {
                    alert('User deleted successfully');
                    fetchUsers(); // Reload users after deletion
                },
                error: function (error) {
                    console.error('Error deleting user:', error);
                    alert('Failed to delete user.');
                }
            });
        }
    }
});


document.getElementById('addManagerBtn').addEventListener('click', function () {
    // Get input values
    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;
    const firstName = document.getElementById("firstName").value;
    const lastName = document.getElementById("lastName").value;
    const gender = document.getElementById("gender").value;
    const date = document.getElementById("date").value;
    const chocolateFactory = document.getElementById("chocolateFactory").value;

    // Validate the input data
    if (username === "" || password === "" || firstName === "" || lastName === "") {
        alert("Please fill in all fields correctly.");
        return;
    }

    // Create the factory object to send
    const factoryData = {
        username: username,
        password: password,
        firstName: firstName,
        lastName: lastName,
        gender: gender,
        date: date,
        chocolateFactory: null
    };

    // Send a POST request to create the factory
    fetch('http://localhost:8080/web_2024/rest/user/manager/add', {
        method: 'POST', headers: {
            'Content-Type': 'application/json',
        }, body: JSON.stringify(factoryData)
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            alert("Manager created successfully!");
            // Optionally, reload or update the page with the new factory info
            // location.reload();
            managerModal.style.display = "none";
        })
        .catch(error => {
            console.error('There was an error creating the factory:', error);
            alert("Error creating factory, please try again.");
        });
});

function fetchFactoryDetails(factoryName) {

    fetch(`http://localhost:8080/web_2024/rest/chocolate/factory/name?name=${encodeURIComponent(factoryName)}`)
        .then(response => response.json())
        .then(factory => {
            if (factory) {
                displayFactoryDetails(factory);
            } else {
                alert('Factory not found!');
            }
        })
        .catch(error => {
            console.error('Error fetching factory:', error);
        });
}
function displayFactoryDetails(factory) {

    document.getElementById('editFactoryName').setAttribute('value', factory.name);
    document.getElementById('editWorkingHours').setAttribute('value', factory.workingHours);
    document.getElementById('editLocation').setAttribute('value', factory.location.address);
    document.getElementById('editIsWorking').setAttribute('value', factory.isWorking);
    document.getElementById('editLogo').setAttribute('value', factory.logo);
    let select = document.getElementById('editManager');
    const placeholderOption = document.createElement('option');
    placeholderOption.value = factory.manager.username;
    placeholderOption.textContent = factory.manager.firstName + " " + factory.manager.lastName;
    placeholderOption.disabled = true;
    placeholderOption.selected = true;
    select.appendChild(placeholderOption);
    document.querySelector(".factory-manager").style.display = "none";
}

document.getElementById("manager").addEventListener("change", function() {
    const selectedValue = this.value;
    const factoryManagerDiv = document.querySelector(".factory-manager");

    if (selectedValue) {
        // Hide the factory-manager div if a manager is selected
        factoryManagerDiv.style.display = "none";
    } else {
        // Show the factory-manager div if no manager is selected
        factoryManagerDiv.style.display = "block";
    }
});

// Call the function to populate users when the page loads
document.addEventListener('DOMContentLoaded', () => {
    populateManagers()
    fetchFactories()
});