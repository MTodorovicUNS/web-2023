const baseUrl = "http://localhost:8080/web_2024/";

$(document).ready(function () {
    $("#registerBtn").click(function (event) {
        event.preventDefault();
        let username = $('#username').val();
        let password = $('#password').val();
        let firstName = $('#first_name').val();
        let lastName = $('#last_name').val();
        let gender = $('#gender').find(":selected").val();
        let date = $('#birth_date').val();
        let validate = false;

        var regName = new RegExp("([A-Z]{1})([a-z]+)$");
        var regUser = new RegExp("[a-zA-Z0-9]+$");
        if (firstName == "") {
            document.getElementById("eri").removeAttribute("hidden");
        } else {
            document.getElementById("eri").setAttribute("hidden", "true");
            validate = true;
        }
        if (lastName == "") {
            document.getElementById("erp").removeAttribute("hidden");
            validate = false;
        } else {
            document.getElementById("erp").setAttribute("hidden", "true");
            if (validate) validate = true;
        }
        if (date == "") {
            document.getElementById("erd").removeAttribute("hidden");
            validate = false;
        } else {
            document.getElementById("erd").setAttribute("hidden", "true");
            if (validate) validate = true;
        }
        if (!regUser.test(username)) {
            document.getElementById("erk").removeAttribute("hidden");
            validate = false;
        } else {
            document.getElementById("erk").setAttribute("hidden", "true");
            if (validate) validate = true;
        }
        if (!regUser.test(password)) {
            document.getElementById("erl").removeAttribute("hidden");
            validate = false;
        } else {
            document.getElementById("erl").setAttribute("hidden", "true");
            if (validate) validate = true;
        }

        if (validate) {
            $.post({
                url: baseUrl + "rest/auth/register", contentType: "application/json", data: JSON.stringify({
                    username: username,
                    password: password,
                    firstName: firstName,
                    lastName: lastName,
                    gender: gender,
                    date: date,
                }), success: function (odgovor) {
                    window.location.assign("http://localhost:8080/web_2024/login.html");
                }, error: function (odgovor) {
                    if (odgovor.status == 400) {
                        document.getElementById("errorUser").removeAttribute("hidden");
                    } else {
                        document.getElementById("errorUser").setAttribute("hidden", "true");
                        alert("Greska pri registraciji!");
                    }
                },
            });
        }
    });

    $("#loginBtn").click(function (event) {
        event.preventDefault();

        let username = $('#username').val();
        let password = $('#password').val();

        $.post({
            url: baseUrl + "rest/auth/login",
            contentType: "application/json",
            data: JSON.stringify({username: username, password: password}),
            success: function (response) {
                sessionStorage.setItem("jwt", response.jwt);
                localStorage.setItem('user', JSON.stringify(response));
                localStorage.setItem('userRole', response.role);
                if (response.role == "CUSTOMER") {
                    window.location.assign("http://localhost:8080/web_2024/html/customer_page.html");
                } else if (response.role == "ADMIN") {
                    window.location.assign("http://localhost:8080/web_2024/html/admin_page.html");
                } else if (response.role == "MANAGER") {
                    window.location.assign("http://localhost:8080/web_2024/html/manager_page.html");
                } else if (response.role == "WORKER") {
                    window.location.assign("http://localhost:8080/web_2024/html/worker_page.html");
                } else {
                    window.location.assign("http://localhost:8080/web_2024/index.html");
                }
            },
            error: function (response) {
            },
        });
    });
});

function populateFactoryCards() {
    const url = 'http://localhost:8080/web_2024/rest/chocolate/factory/all'; // Replace with your REST API endpoint

    fetch(url)
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json(); // Parse JSON response
        })
        .then(data => {
            const factoriesContainer = document.querySelector('.factories-container');
            factoriesContainer.innerHTML = '';

            data.forEach(factory => {
                const factoryCard = document.createElement('div');
                factoryCard.classList.add('factory-card');

                const factoryImage = document.createElement('img');
                factoryImage.src = factory.logo || 'default-image.png';
                factoryImage.alt = factory.name;

                const factoryName = document.createElement('h3');
                factoryName.textContent = factory.name;

                const factoryLocation = document.createElement('p');
                factoryLocation.textContent = `Location: ${factory.location.address}`;

                const factoryHours = document.createElement('p');
                factoryHours.textContent = `Working Hours: ${factory.workingHours}`;

                const factoryReview = document.createElement('p');
                factoryReview.textContent = `Review: ${factory.review}/5`;

                const viewBtn = document.createElement('button');
                viewBtn.setAttribute("data-name", factory.name)
                viewBtn.textContent = "View";
                viewBtn.classList.add("view-btn");

                factoryCard.appendChild(factoryImage);
                factoryCard.appendChild(factoryName);
                factoryCard.appendChild(factoryLocation);
                factoryCard.appendChild(factoryHours);
                factoryCard.appendChild(factoryReview);
                factoryCard.appendChild(viewBtn);

                // Append the card to the container
                factoriesContainer.appendChild(factoryCard);
            });
            document.querySelectorAll(".view-btn").forEach(button => {
                button.addEventListener("click", function () {
                    const factoryName = this.getAttribute("data-name");
                    window.location.href = `html/view_factory.html?name=${factoryName}`;
                });
            });
        })
        .catch(error => {
            console.error('There was an error fetching the factories:', error);
        });

}

document.getElementById('searchBtn').addEventListener('click', function () {
    const name = document.getElementById('factoryName').value;
    const chocolateName = document.getElementById('chocolateName').value;
    const location = document.getElementById('location').value;
    const rate = document.getElementById('rate').value;

    const queryParams = new URLSearchParams({
        name: name, chocolateName: chocolateName, location: location, rate: rate
    });

    // Fetch request
    fetch(`http://localhost:8080/web_2024/rest/chocolate/factory/search?${queryParams.toString()}`)
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(data => {
            // Handle the search results
            const factoriesContainer = document.querySelector('.factories-container');
            factoriesContainer.innerHTML = '';

            data.forEach(factory => {
                const factoryCard = document.createElement('div');
                factoryCard.classList.add('factory-card');

                const factoryImage = document.createElement('img');
                factoryImage.src = factory.logo || 'default-image.png'; // Use factory image or default
                factoryImage.alt = factory.name;

                const factoryName = document.createElement('h3');
                factoryName.textContent = factory.name;

                const factoryLocation = document.createElement('p');
                factoryLocation.textContent = `Location: ${factory.location}`;

                const factoryHours = document.createElement('p');
                factoryHours.textContent = `Working Hours: ${factory.workingHours}`;

                const factoryReview = document.createElement('p');
                factoryReview.textContent = `Review: ${factory.review}/5`;

                factoryCard.appendChild(factoryImage);
                factoryCard.appendChild(factoryName);
                factoryCard.appendChild(factoryLocation);
                factoryCard.appendChild(factoryHours);
                factoryCard.appendChild(factoryReview);

                factoriesContainer.appendChild(factoryCard);
            });
        })
        .catch(error => {
            console.error('There was a problem with the fetch operation:', error);
            // Handle error
            alert('An error occurred while fetching data.');
        });
});

function logout() {
    localStorage.removeItem('user');
    localStorage.removeItem('userRole');
    window.location.reload();
}

document.getElementById('filterBtn').addEventListener('click', function () {
    const name = document.getElementById('factoryNameFilter').value;
    const location = document.getElementById('locationFilter').value;
    const rating = document.getElementById('ratingFilter').value;
    const sortBy = document.getElementById('sortBy').value;
    const sortOrder = document.getElementById('sortOrder').value;

    const url = new URL('http://localhost:8080/web_2024/rest/chocolate/factory/filter');
    const params = {
        name: name, location: location, rating: rating, sortBy: sortBy, sortOrder: sortOrder
    };

    Object.keys(params).forEach(key => url.searchParams.append(key, params[key]));

    fetch(url)
        .then(response => response.json())
        .then(data => {

            const factoriesContainer = document.querySelector('.factories-container');
            factoriesContainer.innerHTML = '';

            data.forEach(factory => {
                const factoryCard = document.createElement('div');
                factoryCard.classList.add('factory-card');

                // Create image element
                const factoryImage = document.createElement('img');
                factoryImage.src = factory.logo || 'default-image.png';
                factoryImage.alt = factory.name;

                const factoryName = document.createElement('h3');
                factoryName.textContent = factory.name;

                const factoryLocation = document.createElement('p');
                factoryLocation.textContent = `Location: ${factory.location.address}`;

                const factoryHours = document.createElement('p');
                factoryHours.textContent = `Working Hours: ${factory.workingHours}`;

                const factoryReview = document.createElement('p');
                factoryReview.textContent = `Review: ${factory.review}/5`;

                factoryCard.appendChild(factoryImage);
                factoryCard.appendChild(factoryName);
                factoryCard.appendChild(factoryLocation);
                factoryCard.appendChild(factoryHours);
                factoryCard.appendChild(factoryReview);

                factoriesContainer.appendChild(factoryCard);
            });
        })
        .catch(error => {
            console.error('Error fetching data:', error);
        });
});

document.getElementById('logoutLink').addEventListener('click', logout);
document.addEventListener('DOMContentLoaded', populateFactoryCards);
